/* HAPC protocol framing utility code */

/* (C) 2015 by Harald Welte <laforge@gnumonks.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */


#include <osmocom/core/utils.h>

#include "hapc_frame.h"

const struct value_string hapc_flow_names[30] = {
	{ HAPC_FLOW_STATUS,	"status" },
	{ HAPC_FLOW_DEBUG,	"debug" },
	{ HAPC_FLOW_INIT,	"init" },
	{ HAPC_FLOW_WI2C,	"wi2c" },
	{ HAPC_FLOW_TRACE,	"trace" },
	{ HAPC_FLOW_RI2C,	"ri2c" },
	{ HAPC_FLOW_MSG,	"msg" },
	{ HAPC_FLOW_GDB,	"gdb" },
	{ HAPC_FLOW_READ,	"read" },
	{ HAPC_FLOW_READ_PACK,	"read-pack" },
	{ HAPC_FLOW_WRITE,	"write" },
	{ HAPC_FLOW_OBJECT,	"object" },
	{ HAPC_FLOW_RESET,	"reset" },
	{ HAPC_FLOW_PROTOCOL,	"protocol" },
	{ HAPC_FLOW_EEPROM,	"eeprom" },
	{ HAPC_FLOW_EEPROM2,	"eeprom2" },
	{ HAPC_FLOW_FS,		"filesystem" },
	{ HAPC_FLOW_MSG2,	"msg2" },
	{ HAPC_FLOW_KBD,	"kbd" },
	{ HAPC_FLOW_WATCH,	"watch" },
	{ HAPC_FLOW_DIAG,	"diag" },
	{ HAPC_FLOW_RTK,	"rtk" },
	{ HAPC_FLOW_RPC2,	"rpc2" },
	{ HAPC_FLOW_PROD,	"prod" },
	{ HAPC_FLOW_RADIO,	"radio" },
	{ HAPC_FLOW_AT,		"at" },
	{ HAPC_FLOW_TRACE_TSTP,	"trace" },
	{ HAPC_FLOW_DUMP,	"dump" },
	{ HAPC_FLOW_ATENCAP,	"atencap" },
	{ 0, NULL }
};

const struct value_string hapc_obj_status_names[9] = {
	{ HAPC_OBJ_READ_REQ,	"read-req" },
	{ HAPC_OBJ_READ_RESP,	"read-resp" },
	{ HAPC_OBJ_WRITE_REQ,	"write-req" },
	{ HAPC_OBJ_WRITE_RESP,	"write-resp" },
	{ HAPC_OBJ_ID_LIST_REQ,	"id-list-req" },
	{ HAPC_OBJ_ID_LIST_RESP,"id-list-resp" },
	{ HAPC_OBJ_DEL_REQ,	"delete-req" },
	{ HAPC_OBJ_DEL_RESP,	"delete-resp" },
	{ 0, NULL }
};

