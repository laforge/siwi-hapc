/* HAPC protocol framing test code */

/* (C) 2015 by Harald Welte <laforge@gnumonks.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#include <stdio.h>

#include "hapc_frame.h"

const uint8_t test_frame[] = { 0xAA, 0x10, 0x58, 0x02, 0x00, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFC, 0x00, 0x00, 0x00, 0x01, 0x00, 0x13 };
const uint8_t resp_frame[] = { 0xAA, 0x20, 0x58, 0x02, 0x00, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x10, 0x00, 0x10, 0x00, 0x00, 0x00, 0x82,
			       0x18, 0x81, 0x00, 0x00, 0x20, 0x03, 0x00, 0x09, 0x08, 0x91, 0x94, 0x58, 0x16, 0x89, 0x63, 0x07, 0xf2, 0x0d };

static void dump_frame_object(const struct hapc_flash_obj_hdr *foh, unsigned int len)
{
	unsigned int payload_len = len - sizeof(*foh);
	printf("obj_id=0x%08x, mask=0x%08x, total_size=%d, block_size=%d, offset_block=%d, status=%s, data=%s\n",
		foh->obj_id, foh->mask, foh->total_size, foh->block_size, foh->offset_block,
		get_value_string(hapc_obj_status_names, foh->obj_status & 0x7f), osmo_hexdump(foh->data, payload_len));
}

static uint8_t compute_checksum(const uint8_t *data, unsigned int len)
{
	unsigned int i;
	uint8_t checksum = 0;

	for (i = 0; i < len; i++)
		checksum += data[i];

	return checksum;
}

void dump_frame(const struct hapc_msg_hdr *hdr)
{
	unsigned int len = hapc_payload_len(hdr);
	uint8_t csum_frame, csum_computed;

	printf("len=%4u, flow_id=%s: ", len,
		get_value_string(hapc_flow_names, hdr->flow_id));

	csum_frame = hapc_frame_csum(hdr);
	csum_computed = compute_checksum((const uint8_t *) hdr, sizeof(*hdr)+len);

	if (csum_frame != csum_computed) {
		printf("CSUM ERROR (%02x != %02x)\n", csum_frame, csum_computed);
		return;
	}

	switch (hdr->flow_id) {
	case HAPC_FLOW_OBJECT:
		dump_frame_object((const struct hapc_flash_obj_hdr *) hdr->data, len - sizeof(*hdr));
		break;
	default:
		printf("\n");
		break;
	}
}

int main(int argc, char **argv)
{
	const struct hapc_msg_hdr *hdr = (const struct hapc_msg_hdr *) test_frame;
	dump_frame(hdr);

	hdr = (const struct hapc_msg_hdr *) resp_frame;
	dump_frame(hdr);
}
