#pragma once

/* HAPC protocol framing definitions */

/* (C) 2015 by Harald Welte <laforge@gnumonks.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */




#include <stdint.h>

enum hapc_flow_id {
	HAPC_FLOW_STATUS	= 0x00,
	HAPC_FLOW_DEBUG		= 0x01,
	HAPC_FLOW_INIT		= 0x02,
	HAPC_FLOW_WI2C		= 0x03,
	HAPC_FLOW_TRACE		= 0x04,
	HAPC_FLOW_RI2C		= 0x05,
	HAPC_FLOW_MSG		= 0x06,
	HAPC_FLOW_GDB		= 0x07,
	HAPC_FLOW_READ		= 0x08,
	HAPC_FLOW_READ_PACK	= 0x09,
	HAPC_FLOW_WRITE		= 0x0A,
	HAPC_FLOW_OBJECT	= 0x0B,
	HAPC_FLOW_RESET		= 0x0C,
	HAPC_FLOW_PROTOCOL	= 0x0D,
	HAPC_FLOW_EEPROM	= 0x0E,
	HAPC_FLOW_EEPROM2	= 0x0F,
	HAPC_FLOW_FS		= 0x10,
	HAPC_FLOW_MSG2		= 0x11,
	HAPC_FLOW_KBD		= 0x12,
	HAPC_FLOW_WATCH		= 0x14,
	HAPC_FLOW_DIAG		= 0x16,
	HAPC_FLOW_RTK		= 0x18,
	HAPC_FLOW_RPC2		= 0x19,
	HAPC_FLOW_PROD		= 0x1A,
	HAPC_FLOW_RADIO		= 0x1C,
	HAPC_FLOW_AT		= 0x1D,
	HAPC_FLOW_TRACE_TSTP	= 0x1E,
	HAPC_FLOW_DUMP		= 0x20,
	HAPC_FLOW_ATENCAP	= 0x9F,
};

/* HAPC frame format:
 * first byte: 0xAA
 * 2nd byte: LSB of length
 * 3rd byte: MSB of length (3 lower bits) + flow-id (5 upper bits)
 * N bytes payload
 * 2 bytes of checksum
 */

/* common HAPC header */
struct hapc_msg_hdr {
	uint8_t aa;
	uint8_t lsb_len;
	uint8_t msb_len:3,
		flow_id:5;
	uint8_t data[0];
} __attribute__ ((packed));

#define hapc_payload_len(x)	((x)->msb_len << 8 | (x)->lsb_len)

static inline uint8_t hapc_frame_csum(const struct hapc_msg_hdr *mh)
{
	unsigned int payload_len = hapc_payload_len(mh);

	return mh->data[payload_len];
}

enum hapc_obj_status_id {
	HAPC_OBJ_READ_REQ	= 0x01,
	HAPC_OBJ_READ_RESP	= 0x02,
	HAPC_OBJ_READ_RESP2	= 0x82,
	HAPC_OBJ_WRITE_REQ	= 0x03,
	HAPC_OBJ_WRITE_RESP	= 0x04,
	HAPC_OBJ_WRITE_RESP2	= 0x84,
	HAPC_OBJ_ID_LIST_REQ	= 0x05,
	HAPC_OBJ_ID_LIST_RESP	= 0x06,
	HAPC_OBJ_ID_LIST_RESP2	= 0x86,
	HAPC_OBJ_DEL_REQ	= 0x07,
	HAPC_OBJ_DEL_RESP	= 0x08,
	HAPC_OBJ_DEL_RESP2	= 0x88,
};

/* uses HAPC_FLOW_OBJECT */
struct hapc_flash_obj_hdr {
	uint32_t obj_id;
	uint32_t mask;
	uint16_t total_size;
	uint16_t block_size;
	uint16_t offset_block;
	uint8_t obj_status;
	uint8_t data[0];
} __attribute__ ((packed));


/* uses HAPC_FLOW_READ_PACK */
struct hapc_ram_read {
	uint32_t start_addr;
	uint16_t len;
	uint8_t data[0];
} __attribute__ ((packed));

/* uses HAPC_FLOW_WRITE */
struct hapc_ram_write {
	uint8_t pad0[8];
	uint32_t start_addr;
	uint16_t len;
	uint8_t pad1[2];
	uint8_t data[0];
} __attribute__ ((packed));

#include <osmocom/core/utils.h>

extern const struct value_string hapc_flow_names[30];
const struct value_string hapc_obj_status_names[9];
