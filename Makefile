OSMO_CFLAGS:=`pkg-config --cflags libosmocore`
OSMO_LDFLAGS:=`pkg-config --libs libosmocore`

CFLAGS=-g -Wall $(OSMO_CFLAGS)
LDFLAGS=$(OSMO_LDFLAGS)

all: hapc_test

%.o: %.c
	$(CC) $(CFLAGS) -o $@ -c $<


hapc_test: hapc_test.o hapc_frame.o
	$(CC) $(LDFLAGS) -o $@ $^

clean:
	rm -f hapc_test *.o || true
